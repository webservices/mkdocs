FROM python:alpine
LABEL maintainer "CERN IT-CDA-WF <it-dep-cda-wf@cern.ch>"
ARG mkdocs_version=1.0.4
ARG mkdocs_material_version=3.3.0
RUN pip install mkdocs==${mkdocs_version} mkdocs-material==${mkdocs_material_version}